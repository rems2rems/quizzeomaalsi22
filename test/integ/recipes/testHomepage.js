import { expect } from "chai";
import { JSDOM } from "jsdom";
import sinon from "sinon";
import request from "supertest";
import { DbRecipe } from "../../../src/models/DbRecipe.js";
import { createApp } from "../../../src/createApp.js";

describe('Pages tests', async () => {
    
    it('homepage displays recipes', (done) => {
        const find = sinon.stub(DbRecipe, 'find')
        const recipe = {
            _id: "654321",
            name: "Chili con carne",
            originCountry: "Mexico",
            needOven: false,
            ingredients: ["chili", "carne"],
            isExotic: true,
            needSpecificTools: true
        }
        find.returns([recipe])
        const app = createApp().then((app) => {
            request(app)
                .get('/home')
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    const dom = new JSDOM(res.text);
                    const {window:{document}} = dom
                    // console.log(dom.serialize());
                    const title = document.querySelector("h1")
                    expect(title).to.be.ok
                    expect(title.textContent).to.eql("Bienvenue sur Cordon-bleu!")
                    const recipes = document.querySelectorAll(".popularRecipes li")
                    expect(recipes).to.be.ok
                    expect(recipes.length).to.be.greaterThanOrEqual(1)
                    const chili = recipes[0]
                    expect(chili).to.be.ok
                    expect(chili.textContent.trim()).to.eql("Chili con carne - medium")
                    done()
                });
        })
    });
});