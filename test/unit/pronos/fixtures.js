
export function createMatch(){
    return {
        _id: 1,
        home: 1,
        visitors: 2,
        result: {
            halfTime: {
                home: 2,
                visitors: 1
            },
            fullTime: {
                home: 3,
                visitors: 2
            }
        }
    }
}

export function createBet(){
    return {
        _id: 1,
        match: 1,
        better: 1,
        result: {
            halfTime: {
                home: 1,
                visitors: 0
            },
            fullTime: {
                home: 1,
                visitors: 0
            }
        }
    }
}
export function createTournament() {
    return {
        _id: 1,
        name: "Test Cup",
        betters: [
            {
                _id: 1,
                name: "Ana"
            },
            {
                _id: 2,
                name: "Bob"
            }
        ],
        teams: [
            {
                _id: 1,
                name: "Warriors"
            },
            {
                _id: 2,
                name: "Celtics"
            }
        ],
        matches: [
            createMatch(),
            {
                _id: 1,
                home: 2,
                visitors: 1,
                result: {
                    halfTime: {
                        home: 0,
                        visitors: 0
                    },
                    fullTime: {
                        home: 0,
                        visitors: 1
                    }
                }
            }
        ],
        bets: [
            createBet(),
            {
                _id: 1,
                match: 1,
                better: 2,
                result: {
                    halfTime: {
                        home: 0,
                        visitors: 1
                    },
                    fullTime: {
                        home: 1,
                        visitors: 1
                    }
                }
            }
        ]
    }
}