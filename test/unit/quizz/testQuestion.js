import { expect } from 'chai';
import { readFile } from 'fs/promises';
import { createQuestion } from '../../../src/business/quizz/question.js';
const countries = JSON.parse(await readFile(new URL('../../../src/business/quizz/countries.json', import.meta.url)));

describe('Question Tests', () => {
    it('Should create a question from a country', () => {
        const [country,...others] = countries.slice(0,4)
        const question = createQuestion(country,others,{given:"country",answer:"capital"})
        expect(question.given).to.equal("Marshall Islands")
        expect(question.expected).to.equal("Majuro")
        expect(question.choices.length).to.equal(4)
        expect(question.choices).to.include("Majuro")
    });
});