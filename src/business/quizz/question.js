import { shuffle } from "../../util/shuffle.js";

export function createQuestion(country,others,type){
    const question = {}
    if(type.given === "country"){
        question.given = country.name.common
    }
    if(type.answer === "capital"){
        question.choices = shuffle([country.capital[0],...others.map((c)=>c.capital[0])])
        question.expected = country.capital[0]
    }
    return question
}