import { getWinner, hasBeenPlayed } from "./winner.js"

export function createRanking(tournament) {
    const ranking = []
    for (const better of tournament.betters) {
        const rankedBetter = {...better,points: 0 }
        for(const bet of tournament.bets){
            if(bet.better !== rankedBetter._id){
                continue
            }
            // console.log("bet",bet,rankedBetter);
            const match = tournament.matches.find(match => match._id === bet.match)
            // console.log(hasBeenPlayed(match));
            if(!hasBeenPlayed(match)){
                continue
            }
            // console.log(match,getWinner(match),bet,getWinner(bet));
            if(getWinner(match) === getWinner(bet)){
                rankedBetter.points += 2
            }
            if(getWinner(match) !== "none" && getWinner(bet) !== "none" && getWinner(match) !== getWinner(bet)){
                rankedBetter.points -= 1
            }
        }
        ranking.push(rankedBetter)
    }
    return ranking
}